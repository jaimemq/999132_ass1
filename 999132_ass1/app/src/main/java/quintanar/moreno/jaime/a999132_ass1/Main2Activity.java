package quintanar.moreno.jaime.a999132_ass1;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/*For this activity I saw the following tutorials:
Code referred to SQLite: https://www.youtube.com/watch?v=cp2rL3sAFmI&list=PLS1QulWo1RIaRdy16cOzBO5Jr6kEagA07
Code referred to Address given the coordinates:
https://www.youtube.com/watch?v=66F69bqAups

In this activity we are going to store the data in the table of our database.
* We get the coordinates from the first activity and we have to include the name of
* the place. We write the information and when we press ADD DATA button, we include the element
* in the table. With VIEW ALL we can see all the position we have registered.
* It implements the OnClickListener*/

public class Main2Activity extends AppCompatActivity implements View.OnClickListener{
    /*VARIABLE DECLARATION
    * We need two buttons to ADD DATA and VIEW ALL functionalities, three EditText where
    * we will store longitude, latitude and name of the place.
    * We include another button WHERE ARE YOU? to get your address given your coordinates and
    * a RequestQueue*/
    DataBaseHelper firstDb;
    EditText editLongitude;
    EditText editLatitude;
    EditText namePlace;
    Button button3;
    Button button4;
    Button button5;
    String longitudeString;
    String latitudeString;
    private RequestQueue requestQueue;

    /*We create one DataBaseHelper to use the methods we implemented on DataBaseHelper class.
    * We initiate all the variables and use intent and bundle to get the data we are sending
    * from the first activity. We edit editLongitude and editLatitude with the new data*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        firstDb = new DataBaseHelper(this);
        editLongitude = (EditText) findViewById(R.id.editText);
        editLatitude = (EditText) findViewById(R.id.editText2);
        namePlace = (EditText) findViewById(R.id.editText3);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        requestQueue = Volley.newRequestQueue(this);


        button3.setOnClickListener(this);
        button4.setOnClickListener(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            longitudeString = (String) bundle.get("LONGITUDE");
            editLongitude.setText(longitudeString);
            latitudeString = (String) bundle.get("LATITUDE");
            editLatitude.setText(latitudeString);
        }


        /*We configure the button5 (WHERE ARE YOU?) to get the address given the coordinates
        * when it pressed, with a OnClickListener.
        * With a JasonObjectRequest object and given the coordinates in the URL
        * that we include as parameter in the object constructor, we obtain the address.
        * store the request on our requestQueue*/
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonObjectRequest request = new JsonObjectRequest("https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitudeString+","+longitudeString+"&key=AIzaSyBgBCqaZaO24008SSMOBHUmNL5KA1t3J9I", new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String address = response.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                            namePlace.setText(address);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                });
                requestQueue.add(request);
            }
        });
    }

    /*With the first button we introduce in the table one element. If the element couldn't be
    * inserted for any reason or if the element is well inserted, a toast is shown to tell us
    * how was the insertion.
    * With the second button we show all the elements stored in the buffer, in the database.
    * If no elements, a message is shown in the screen*/
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button3:
                boolean isInserted = firstDb.insertData(editLongitude.getText().toString(),
                        editLatitude.getText().toString(), namePlace.getText().toString());
                if(isInserted){
                    Toast.makeText(Main2Activity.this, "Data Inserted", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(Main2Activity.this, "Data NOT Inserted", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.button4:
                Cursor res = firstDb.getAllData();
                if(res.getCount() == 0){
                    showMessage("Error", "Nothing found");
                    return;
                }else{
                    StringBuffer buffer = new StringBuffer();
                    while(res.moveToNext()){
                        buffer.append("Id : " + res.getString(0) + "\n");
                        buffer.append("Longitude : " + res.getString(1) + "\n");
                        buffer.append("Latitude : " + res.getString(2) + "\n");
                        buffer.append("Name: " + res.getString(3) + "\n\n");
                    }
                    showMessage("Last places you have visited", buffer.toString());
                }
                break;
            default:
                break;
        }
    }
    /*This is the method in charge of showing the table*/
    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}
