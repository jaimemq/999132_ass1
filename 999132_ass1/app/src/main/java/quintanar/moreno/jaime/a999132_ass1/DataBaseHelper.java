package quintanar.moreno.jaime.a999132_ass1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/*For this class I followed the following tutorial:
https://www.youtube.com/watch?v=cp2rL3sAFmI&list=PLS1QulWo1RIaRdy16cOzBO5Jr6kEagA07, from the first
video to the fourth.

DataBaseHelper is the class where we are going to implement the methods we need
* for create our database and store elements inside*/
public class DataBaseHelper extends SQLiteOpenHelper{

    /*VARIABLES DECLARATION
    * DATABASE_NAME will be the name what our database file will have
    * TABLE_NAME is the name of the table inside our database where we will store the elements
    * Our Table will have 4 fields: ID, Longitude, latitude and name of the place
    * We are not going to include a COL_1="ID" because in the method onCreate, we will create an
    * autoincrement key that automatically generates the position of each element*/
    public static final String DATABASE_NAME = "Locations.db";
    public static final String TABLE_NAME = "locations_table";
    public static final String COL_2 = "LONGITUDE";
    public static final String COL_3 = "LATITUDE";
    public static final String COL_4 = "PLACE";


    /*DataBaseHelper constructor*/
    public DataBaseHelper(Context context) {
        super(context,DATABASE_NAME, null, 1);
    }

    /*execSQL method execute a single SQL statement that is NOT a SELECT
    or any other SQL statement that returns data. We include the type of each variable. With the
    string ID INTEGER PRIMARY KEY AUTOINCREMENT, our ID will be automatically incremented */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " LONGITUDE DOUBLE, LATITUDE DOUBLE, PLACE STRING)");
    }

    /*This method drops the table*/
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /*Method to insert the elements in our table. How the Id is automatically incremented,
    * we only have to include the longitude, latitude and place
    * With getWritableDatabase() we create and/or open a database that will be used
    * for reading and writing.
    * We create a new ContentValues object where we'll store the values for longitude,
    * latitude, place for each element in the table. Then we insert contentValues
    * on the table*/
    public boolean insertData(String longitude, String latitude, String place){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, longitude);
        contentValues.put(COL_3, latitude);
        contentValues.put(COL_4, place);
        long result = db.insert(TABLE_NAME, null, contentValues);//Insert() return -1 in case of error
        if (result == -1){
            return false;
        }else{
            return true;
        }

    }

    /*This method shows all the data stored in our table*/
    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        return res;
    }

}
