package quintanar.moreno.jaime.a999132_ass1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*For this activity I saw the following tutorials:
Code referred to get your coordinates: https://www.youtube.com/watch?v=QNb_3QKSmMk
Code to widget and general uses of Android Studio:
https://www.youtube.com/watch?v=3IvfKtgFVMc&list=PLh_neeN4BQCmX4jC9pR_DgZVJ8a4PRMca (first 20 videos)

In this first activity we have the following actions: Request Location, that gives us the
* coordinates of our actual position each five seconds and show them in the screen; Register
* your position, that brings you to the second activity.
* This class implements the OnClickListener interface*/
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /*VARIABLE DECLARATION
    We declare the variables we are going to use in the application: A button which get your
    position when is pressed, a text view where your coordinates will be showed (scroll view
    is used to see all the list), location manager to access to the system location services,
    location listener used for receiving notifications from the LocationManager
    when the location has changed, button2 to go to the second activity. Variables longitude and
    latitude are created to store those data and send them to the second activity, to store them
    in the database
    */
    private Button button;
    private TextView textView;
    private LocationManager locationManager;
    private LocationListener listener;
    Button button2;
    private double longitude;
    private double latitude;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*VARIABLE INITIATION
        We have to create a new LocationListener object and implement some methods:
        onLocationChanged we include the text we want to see, that is the longitude and latitude;
        onStatusChanged and onProviderEnabled are not necessary to implement for this app;
        onProviderDisabled to configure settings to allow configuration of current location sources,
        and start that settings (startActivity())
        */
        textView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);

        button2.setOnClickListener(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        listener = new LocationListener() {
            @Override
            /*I create the text that is going to be shown on the screen
            * I want to save the first updating values and try to store them
            * in SQLite database. I use the variables longitude and latitude to store the
            * coordinates of my position*/
            public void onLocationChanged(Location location) {
                textView.append("\n " + "Longitude: " + location.getLongitude() + "\nLatitude: "
                        + location.getLatitude() + "\n ");
                longitude = location.getLongitude();
                latitude = location.getLatitude();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };

        /*Once we have initiate every variable, we have to make the button works properly, that is,
        give your location when it's pressed. We use configure_button()
        */
        configure_button();
    }

    /*If the permissions are accepted, we configure the button*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 10:
                configure_button();
                break;
            default:
                break;
        }
    }

    /*configure_button()
     First we check the permissions to use tha app in our phone: check the version API and the GPS.
     Then, if permissions are allowed (if not allowed, the if loop above call return),
     we make with the onClickListener that the app register your position when you push
     the button.
     With requestLocationUpdates we call the location each 5s
    */
    void configure_button(){
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.INTERNET}
                        ,10);
            }
            return;
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //noinspection MissingPermission
                locationManager.requestLocationUpdates("gps", 5000, 0, listener);

            }
        });
    }

    /*This method is the default method of the OnClickListener we have to implement. We use it
    * for going to the second activity. We send to the second activity the variables longitude
    * and latitude*/
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button2:
                Intent intent = new Intent(this, Main2Activity.class);
                String longitudeString = String.valueOf(longitude);
                String latitudeString = String.valueOf(latitude);

                intent.putExtra("LONGITUDE", longitudeString);
                intent.putExtra("LATITUDE", latitudeString);

                startActivity(intent);
                break;
            default:
                break;
        }
    }
    /*We implement this method to avoid the GPS keeps updating your position
    * when you quit the app. (code taken from StackOverFlow:
    * http://stackoverflow.com/questions/4197478/how-to-stop-location-manager)*/
    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(listener);
    }
}
